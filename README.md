# README #

Kent Express are the UK’s biggest mail order dental supplier. Since 1982 we’ve offered UK high street dentists outstanding value on everyday dental consumables, small equipment and uniform from the leading brands.

We have one of the most experienced account management teams in the country, who will take time to get to know you and your business. With an order fulfillment rate of over 99%, we offer next day delivery as standard with unparalleled reliability. You’ll get what you need, when you need it. Plus we offer an extra 1.5% discount on every online order.

We understand your challenges and offer flexibility where needed. For example, regular customers have the opportunity to fix their buying prices for six. Finance deals are available on larger orders to help you spread the cost (subject to status).

Our aim is to make your life easier through our expertise, partnership and service. Whether you’re looking for NiTi files or Nitrile gloves, we work with the world’s leading manufacturers to bring you fantastic deals every day. From 3M to Dentsply, we’ve got it covered.

With Kent Express you can be confident of a great customer experience. For help with our website, call our dedicated helpdesk on 0800 032 7922 (Monday to Friday 9am-5pm) or email ecommerce@kentexpress.co.uk For sales, please call 0800 028 1181 (Monday to Friday 8.30am-5.30pm) or email sales@kentexpress.co.uk

https://www.kentexpress.co.uk